;; Améliorer le temps de démarrage :
(setq gc-cons-threshold 100000000)

;; Gestion de packages :
(package-initialize)
;; Choix et priorité des dépôts :
(add-to-list 'package-archives
	     '("melpa" . "http://melpa.org/packages/"))
(setq package-archive-priorities '(("gnu" . 8)
                                   ("melpa" . 10)))

;; Utilisation de use-package :
(unless (package-installed-p 'use-package)
  (package-refresh-contents)
  (package-install 'use-package))
(eval-when-compile
  (require 'use-package))

;; Démarrer en plein écran :
(add-to-list 'initial-frame-alist '(fullscreen . maximized))

;; Désactiver l'écran d'accueil :
(setq inhibit-splash-screen t)

;; Affichage des numéros de ligne dans la marge de gauche :
(global-display-line-numbers-mode)

;; Doom-modeline :
(use-package doom-modeline
  :ensure t
  :init (doom-modeline-mode 1)
  :config
  (setq doom-modeline-continuous-word-count-modes '(markdown-mode gfm-mode org-mode))
  (setq doom-modeline-minor-modes t)
  (setq doom-modeline-buffer-state-icon t)
  (setq doom-modeline-enable-word-count t))

(use-package all-the-icons
  :ensure t)

;; Amélioration de la complétion et du minibuffer :
(use-package vertico
  :ensure t
  :init
  (vertico-mode)
  :config
  (setq vertico-sort-function 'vertico-sort-alpha))

(use-package marginalia
  :ensure t
  :bind (:map minibuffer-local-map
              ("M-A" . marginalia-cycle))
  :init
  (marginalia-mode))

(use-package orderless
  :ensure t
  :config
  (setq completion-styles '(basic partial-completion orderless))
  ;; (completion-category-defaults nil)
  (setq orderless-matching-styles '(orderless-regexp)))

;; Utilisation de l'encodage UTF-8 :
(set-terminal-coding-system 'utf-8)
(set-keyboard-coding-system 'utf-8)
(prefer-coding-system 'utf-8)

;; Activer le mode de suppression de la région active :
(delete-selection-mode 1)

;; Réglages généraux d'édition :
(setq show-paren-delay 0)
(show-paren-mode 1)
(global-hl-line-mode 1)
(setq-default word-wrap t)
(toggle-truncate-lines -1)

;; Backups automatiques :
(setq backup-directory-alist
          `((".*" . ,temporary-file-directory)))
(setq auto-save-file-name-transforms
          `((".*" , temporary-file-directory t)))

;; Vérification orthographique en mode texte :
(add-hook 'text-mode-hook 'flyspell-mode)

;; Réglages pour LaTeX :
(use-package tex
  :ensure auctex
  :defer t
  :init
  (defun tex-pdf-on () (TeX-PDF-mode 1)) ;; fonction utilisée plus bas
  :config
  ;; Réglages conseillés par le manuel de AucTeX :
  (setq TeX-auto-save t)
  (setq TeX-parse-self t)
  (setq-default TeX-master nil)
  ;; Activer par défaut le PDF avec LaTeX :
  (add-hook 'tex-mode-hook 'tex-pdf-on)
  (add-hook 'latex-mode-hook 'tex-pdf-on)
  (setq TeX-PDF-mode t)
  ;; Activer le mode auto-fill pour LaTeX :
  (add-hook 'tex-mode-hook 'turn-on-auto-fill)
  (add-hook 'LaTeX-mode-hook 'turn-on-auto-fill)
  ;; Activer le visual-line-mode pour LaTeX :
  (add-hook 'LaTeX-mode-hook 'visual-line-mode)
  ;; Activer le support de reftex :
  (require 'reftex)
  (add-hook 'LaTeX-mode-hook 'reftex-mode)
  (setq reftex-plug-into-AUCTeX t)
  ;; Vérification orthographique en mode TeX :
  (add-hook 'LaTeX-mode-hook 'flyspell-mode)
  ;; Prettification des symboles :
  (add-hook 'LaTeX-mode-hook 'prettify-symbols-mode)
  ;; Activer le mode math par défaut :
  (add-hook 'LaTeX-mode-hook 'LaTeX-math-mode)
  ;; Toujours exporter avec shell-escape :
  (setq LaTeX-command-style '(("" "%(PDF)%(latex) %(file-line-error) %(extraopts) -shell-escape %S%(PDFout)")))
  ;; Activer la coloration des macros natbib :
  (add-hook
   'LaTeX-mode-hook
   (lambda ()
     (font-latex-add-keywords '(("citep" "*[[{")) 'reference)
     (font-latex-add-keywords '(("citet" "*[[{")) 'reference)
     (font-latex-add-keywords '(("citeyear" "*[[{")) 'reference)
     (font-latex-add-keywords '(("citeauthor" "*[[{")) 'reference)
     (font-latex-add-keywords '(("citealp" "*[[{")) 'reference))))

;; Références biblio :
(use-package bibtex-completion
  :ensure t)

(use-package helm-bibtex
  :ensure t
  :defer t)

(use-package citar
  :ensure t
  :no-require
  :custom
  (org-cite-insert-processor 'citar)
  (org-cite-follow-processor 'citar)
  (org-cite-activate-processor 'citar)
  (citar-bibliography org-cite-global-bibliography)
  :config
  (global-set-key (kbd "<f12>") 'citar-insert-citation))

;; Markdown :
(use-package markdown-mode
  :ensure t
  :defer t
  :mode (("README\\.md\\'" . gfm-mode)
         ("\\.md\\'" . markdown-mode)
         ("\\.Rmd\\'" . markdown-mode))
  :config
  (setq markdown-enable-math t))

;; Pandoc :
(use-package pandoc-mode
  :ensure t
  :config
  (add-hook 'markdown-mode-hook 'pandoc-mode))

;; Quarto :
(use-package quarto-mode
  :ensure t
  :mode (("\\.qmd\\'" . poly-quarto-mode)))

;; Polymode:
(use-package poly-markdown
  :ensure t)

;; Réglages perso pour les PDF :  
(defun fs/display-pdf ()
  "Display the PDF file associated to a given Md, Org or TeX BUFFER.
The PDF is displayed in a new (side, right) window."
  (interactive)
  (let* ((curbuf (current-buffer))
         (bufname (buffer-name curbuf)))
    (cond ((string-match-p ".md$" bufname)
           (find-file (concat "./"
                              (replace-regexp-in-string
                               ".md"
                               ".pdf"
                               bufname))))
          ((string-match-p ".org$" bufname)
           (find-file (concat "./"
                              (replace-regexp-in-string
                               ".org"
                               ".pdf"
                               bufname))))
          ((string-match-p ".tex$" bufname)
           (find-file (replace-regexp-in-string
                       ".tex"
                       ".pdf"
                       (concat "./" (TeX-master-file t))))))
    (split-window-right)
    (switch-to-buffer curbuf)))
(global-set-key (kbd "<f7>") 'fs/display-pdf)
(add-hook 'doc-view-mode-hook 'auto-revert-mode)

;; Variables "safe" :
(push '(citar-bibliography . "bibliography.bib")
      safe-local-variable-values)
(push '(citar-bibliography . "biblio.bib")
      safe-local-variable-values)
