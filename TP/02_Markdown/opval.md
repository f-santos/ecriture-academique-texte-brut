---
title: L'opération Valentine
date: 9 avril 2024
author:
  - John Doe
  - Jane Doe
subtitle: Un extrait d'article Wikipedia
lang: fr
abstract: Nous résumons ici l'occupation des Îles Féroé par l'armée britannique durant la seconde guerre mondiale.
abstract-title: Résumé
classoption: landscape
---

# Introduction
L'occupation britannique des îles Féroé durant la Seconde Guerre
mondiale est connue sous le nom d'"opération Valentine".

# Déroulement
## Une occupation préventive
En avril 1940, le Royaume-Uni occupe les îles Féroé afin de prévenir
une invasion allemande.

Avant l'occupation, les îles Féroé ont le statut d'un *amt* du
Danemark.

Winston Churchill déclara :

> Nous occupons en ce moment les îles Féroé, qui appartiennent au
> Danemark et sont un point stratégique de haute importance.

La radio **BBC** diffuse la nouvelle.

## Arrivée de la Royal Navy
Le 12 avril, deux destroyers de la Royal Navy arrivent au port de
Tórshavn.

![Le port de Torshavn.](port.jpg)

Deux cent cinquante Royal Marines sont débarqués, remplacés plus tard
par d'autres troupes britanniques [@Miller03].

## Les pertes

Les pertes en matériel[^1] durant l'occupation, causées par
l'armée allemande, sont résumées dans la table ci-dessous.

|         | Féroé | Grande-Bretagne |
|:-------:|:-----:|:---------------:|
| Navires | 10    | 3               |
| Avions  | 0     | 2               |

: Pertes en matériel.

# Pour aller plus loin
- [Consulter l'article complet sur Wikipedia.](https://fr.wikipedia.org/wiki/Occupation_britannique_des_%C3%AEles_F%C3%A9ro%C3%A9)
- La société féroïenne a profondément changé, en un laps de temps
  court, suite au contact avec les soldats britanniques. Un roman
  féroïen célèbre [@bru2011_OldManHis] traite des évolutions
  culturelles entre l'ancienne et la nouvelle génération dans
  l'archipel.
  
# Références {-}

[^1]: Ces chiffres sont totalement fictifs.
